screen = '';
key = document.getElementsByClassName('generic');
texto = '';

function getText(texto){
    document.getElementById('screen').value += texto;
}

function extractNumbers(texto, indice, left=false){
    index = undefined
    numero = []
    // indice -- until indice = 0
    if(!left){
        indice = indice + 1
        for(var i=indice; i<texto.length; i++){
            if(texto[i].match(/\d/)){
                numero.push(texto[i])
                index = i 
            }else{
                break;
            }
        }
    }else{
        indice = indice -1
        for(var i=indice; i>=0; i--){
            if(texto[i].match(/\d/)){
                console.log("match")
                numero.push(texto[i])
                index = i
            }else{
                break;
            }
        }
    }

    if(left){
        numero = numero.reverse().join("")
    }else{
        numero = numero.join("")
    }
    return [numero, index]
}

function replaceRange(texto, inicio, fin, replace) {
    text = texto.substring(0, inicio) + replace + texto.substring(fin);
    return text
}

    
function calculate(){
    var screen = document.getElementById('screen').value;
    // console.log(result)
    result =  toCalculate(screen);
    document.getElementById('screen').value = Number(result).toFixed(2);  
}

// watcher for key variable events and print value
window.addEventListener('mouseup', function(e) {
    if(e.target.classList.contains('generic')){
        if(e.target.innerHTML =='AC'){
            this.document.getElementById('screen').value ='';
        }else{
            getText(e.target.innerHTML);
        }
    }else if(e.target.id == 'result'){
        calculate();
    }
});

function validateOperators(digits){
    let operators = [];
    let operadoresValidate = ['*', '/', '+', '-'];
    digits = digits.split("")

    operadoresValidate.forEach(el => {
        // if digits doesnt contain operador then remove from operadores
        if(digits.indexOf(el) != -1){
        //    set element on list of operadores
            operators.push(el);
        }
    });
    return operators;
}


function toCalculate(digits){
    let screenResult = 0;
    let operators = [];
    
    if(digits.length == 0){
        return;
    }else{
        operators = validateOperators(digits);

        for(var i=0; i<operators.length; i++){
            for(var j=0; j<digits.length; j++){
                if(digits.length == undefined){
                    return screenResult 
                }
                if(digits[j] == operators[i]){
                    // operar 
                    let operador = operators[i];
                    let elementoA = extractNumbers(digits,j,true)
                    let elementoB = extractNumbers(digits,j)
                    let a = elementoA[0]
                    let b = elementoB[0]
                    let from = elementoA[1]
                    let to = elementoB[1]

                    switch(operador){
                        case "*":
                            result = a * b  
                            break;
                        case "/":
                            result = Number(a) / Number(b)
                            break;
                        case "+":
                            result = Number(a) + Number(b)
                            break;
                        case "-":
                            result = Number(a) - Number(b)
                            break;
                    }
                    
                    digits = replaceRange(digits, from, to+1, result);
                    screenResult = result
                }else{
                    continue;
                }
            }   
        }
        this.operators = [];
        return screenResult;
    }
}